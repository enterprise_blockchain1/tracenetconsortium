#!/bin/bash

# Sets the context for native peer commands
function usage {
    echo "Usage: . ./set_peer_env.sh ORG_NAME"
    echo "Sets the organization context for native peer execution"
}

if [ "$1" == "" ]; then
    usage
    exit 1
fi

ORG_CONTEXT=$1
MSP_ID="$(tr '[:lower:]' '[:upper:]' <<< ${ORG_CONTEXT:0:1})${ORG_CONTEXT:1}"
ORG_NAME=$MSP_ID
CORE_PEER_LOCALMSPID=$ORG_NAME"MSP"
FABRIC_LOGGING_SPEC=INFO
FABRIC_CFG_PATH=/workspaces/tracenetconsortium/config/cit
CORE_PEER_ADDRESS=producer1.producetrace.com:7051
CORE_PEER_MSPCONFIGPATH=/workspaces/tracenetconsortium/config/crypto-config/peerOrganizations/producetrace.com/users/Admin@producetrace.com/msp
ORDERER_ADDRESS=orderer.producetrace.com:7050
CORE_PEER_TLS_ENABLED=false

# Debugging output
echo "ORG_CONTEXT: $ORG_CONTEXT"
echo "MSP_ID: $MSP_ID"
echo "ORG_NAME: $ORG_NAME"
echo "CORE_PEER_LOCALMSPID: $CORE_PEER_LOCALMSPID"
echo "FABRIC_LOGGING_SPEC: $FABRIC_LOGGING_SPEC"
echo "FABRIC_CFG_PATH: $FABRIC_CFG_PATH"
echo "CORE_PEER_ADDRESS: $CORE_PEER_ADDRESS"
echo "CORE_PEER_MSPCONFIGPATH: $CORE_PEER_MSPCONFIGPATH"
echo "ORDERER_ADDRESS: $ORDERER_ADDRESS"
echo "CORE_PEER_TLS_ENABLED: $CORE_PEER_TLS_ENABLED"

# Check if peer has joined any channels
peer channel list
